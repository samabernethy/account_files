public abstract class Account implements Detailable {
    double balance;
    String name;
    static double interestRate;

    // Getters and Setters
    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static double getInterestRate() {
        return interestRate;
    }

    public static void setInterestRate(double interestRate) {
        Account.interestRate = interestRate;
    }

    // Constructor
    public Account(double balance, String name) {
        this.balance = balance;
        this.name = name;
    }

    public Account() {
        this.balance = 50;
        this.name = "Sam";
    }

    // methods
    abstract void addInterest();

    public boolean withdraw(double amount) {
        if (amount <= getBalance()) {
            System.out.println("Withdrawing:" + amount);
            setBalance(this.balance - amount);
            System.out.println("New Balance:" + getBalance());
            return true;
        } else {
            System.out.println("Not enough funds");
            return false;
        }
    }

    public boolean withdraw() {
        if (getBalance() >= 20) {
            setBalance(this.balance - 20);
            return true;
        } else {
            System.out.println("Insufficient Funds");
            return false;
        }
    }

    @Override
    public String getDetails() {
        
        return  "Name:" + getName() + ", Balance: " + getBalance()  + ", Interest Rate: " + getInterestRate();
    }
    
    

   

}