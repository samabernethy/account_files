public class SavingsAccount extends Account {

    public SavingsAccount(double balance, String name){
        super(balance, name);
        this.balance = balance;
        this.name = name;
    }

    @Override
    public void addInterest() {
        
        setBalance(getBalance() * 1.4);
        
    }
}
