public class TestInheritance {
    public static void main(String[] args){
        Account[] accs = new Account[3];
        SavingsAccount acc1 = new SavingsAccount(2, "Sam");
        SavingsAccount acc2 = new SavingsAccount(4, "Will");    
        CurrentAccount acc3 = new CurrentAccount(6, "Brian");
        accs[0] = acc1;
        accs[1] = acc2;
        accs[2] = acc3;
        

        for(int i = 0; i <3; i++){
                System.out.println("Account No:" + (i+1));
                System.out.println("Name: " + accs[i].getName());
                System.out.println("Balance: " + accs[i].getBalance());
                System.out.println("Adding Interest: ");
                accs[i].addInterest();
                System.out.println("Balance: " + accs[i].getBalance());

        } 
 
    }
}
