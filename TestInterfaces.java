public class TestInterfaces {
    public static void main(String[] args) {
        Detailable[] detArr = new Detailable[3];
        SavingsAccount acc1 = new SavingsAccount(4, "Will");    
        CurrentAccount acc2= new CurrentAccount(6, "Brian");
        Account.setInterestRate(1.2);
        HomeInsurance homeIns1 = new HomeInsurance(2000, 1000, 8000);

        detArr[0] = acc1;
        detArr[1] = acc2;
        detArr[2] = homeIns1;

        print(detArr);



    }

    public static void print(Detailable[] list){
        for(int i=0; i<list.length; i++){
            System.out.println(list[i].getDetails());
        }

    }
}
